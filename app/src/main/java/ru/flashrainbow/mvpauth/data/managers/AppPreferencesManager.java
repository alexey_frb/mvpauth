package ru.flashrainbow.mvpauth.data.managers;

import android.content.SharedPreferences;

import ru.flashrainbow.mvpauth.R;
import ru.flashrainbow.mvpauth.utils.ConstantManager;
import ru.flashrainbow.mvpauth.utils.DevApplication;

/**
 * Сохранение/загрузка пользовательских данных, используя shared preferences
 */
public class AppPreferencesManager {

    private SharedPreferences mSharedPreferences;

    public AppPreferencesManager() {
        this.mSharedPreferences = DevApplication.getSharedPreferences();
    }

    /**
     * Сохранить токен авторизации в shared preferences
     *
     * @param authToken - токен
     */
    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }

    /**
     * Получить токен авторизации из shared preferences
     *
     * @return - токен
     */
    public String getAuthToken() {
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN_KEY, DevApplication.getContext().getString(R.string.default_value_auth_token));
    }
}
