package ru.flashrainbow.mvpauth.data.managers;

import android.content.Context;

import ru.flashrainbow.mvpauth.utils.DevApplication;

public class DataManager {

    private static DataManager INSTANCE = null;

    private AppPreferencesManager mAppPreferencesManager;
    private Context mContext;

    private DataManager() {
        mAppPreferencesManager = new AppPreferencesManager();
        mContext = DevApplication.getContext();
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public AppPreferencesManager getAppPreferencesManager() {
        return mAppPreferencesManager;
    }

    public Context getContext() {
        return mContext;
    }
}