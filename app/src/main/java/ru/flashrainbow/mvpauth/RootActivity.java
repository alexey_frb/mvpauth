package ru.flashrainbow.mvpauth;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.flashrainbow.mvpauth.mvp.presenters.AuthPresenter;
import ru.flashrainbow.mvpauth.mvp.presenters.IAuthPresenter;
import ru.flashrainbow.mvpauth.mvp.views.IAuthView;
import ru.flashrainbow.mvpauth.ui.custom_views.AuthPanel;

public class RootActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {
    AuthPresenter mPresenter = AuthPresenter.getInstance();

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;
    @BindView(R.id.twitter_btn)
    ImageButton mTwitterBtn;
    @BindView(R.id.fb_btn)
    ImageButton mFbBtn;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrap;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordWrap;
    @BindView(R.id.app_name_txt)
    TextView mAppNameTxt;

    private ProgressDialog mProgressDialog;


    //region ----- Life Cycle -----
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        ButterKnife.bind(this);

        mPresenter.takeView(this);
        mPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);
        mVkBtn.setOnClickListener(this);
        mTwitterBtn.setOnClickListener(this);
        mFbBtn.setOnClickListener(this);

        Typeface customFont = Typeface.createFromAsset(getAssets(), getString(R.string.bebas_neue_book));
        mAppNameTxt.setTypeface(customFont);
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    //region ----- IAuthView-----
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("что то пошло не так, попробуйте позже");
            // TODO: 22.10.2016 send error stacktrace to crashlytics
        }
    }

    /**
     * Отобразить прогресс-бар
     */
    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.progress_splash);
    }

    /**
     * Скрыть прогресс-бар
     */
    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public void showErrorEmailEt() {
        mEmailWrap.setErrorEnabled(true);
        mEmailWrap.setError(getString(R.string.error_incorrect_email));
    }

    @Override
    public void hideErrorEmailEt() {
        mEmailWrap.setErrorEnabled(false);
        mEmailWrap.setError("");
    }

    @Override
    public void showErrorPasswordEt() {
        mPasswordWrap.setErrorEnabled(true);
        mPasswordWrap.setError(getString(R.string.error_incorrect_password));
    }

    @Override
    public void hideErrorPasswordEt() {
        mPasswordWrap.setErrorEnabled(false);
        mPasswordWrap.setError("");
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }
    //endregion

    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.vk_btn:
                mPresenter.clickOnVk();
                break;
            case R.id.twitter_btn:
                mPresenter.clickOnTwitter();
                break;
            case R.id.fb_btn:
                mPresenter.clickOnFb();
                break;
        }
    }
}
