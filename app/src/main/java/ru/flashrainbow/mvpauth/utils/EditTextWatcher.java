package ru.flashrainbow.mvpauth.utils;

import android.text.Editable;
import android.text.TextWatcher;

import ru.flashrainbow.mvpauth.mvp.presenters.AuthPresenter;

public class EditTextWatcher implements TextWatcher {

    private AuthPresenter mPresenter = AuthPresenter.getInstance();
    private int mTypeField;

    public EditTextWatcher(int typeField) {
        mTypeField = typeField;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        switch (mTypeField) {
            case ConstantManager.TYPE_FIELD_EMAIL:
                mPresenter.checkInputEmailString(ConstantManager.emailPattern, s.toString());
                break;
            case ConstantManager.TYPE_FIELD_PASSWORD:
                mPresenter.checkInputPasswordString(s.toString());
                break;
        }
    }
}
