package ru.flashrainbow.mvpauth.utils;

/**
 * Константы
 */
public interface ConstantManager {
    String TAG_PREFIX = "DEV ";

    String emailPattern = "^[\\w\\.\\-]{3,}@[A-Za-z0-9\\-]{2,}\\.[A-Za-z]{2,3}$";

    int TYPE_FIELD_EMAIL = 1;
    int TYPE_FIELD_PASSWORD = 2;

    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
}

