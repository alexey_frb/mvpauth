package ru.flashrainbow.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.flashrainbow.mvpauth.mvp.views.IAuthView;

public interface IAuthPresenter {

    void takeView(IAuthView authView);

    void dropView();

    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();

    void clickOnFb();

    void clickOnVk();

    void clickOnTwitter();

    void clickOnShowCatalog();

    boolean checkUserAuth();

    void checkInputEmailString(String patternString, String inputString);

    void checkInputPasswordString(String inputString);
}
