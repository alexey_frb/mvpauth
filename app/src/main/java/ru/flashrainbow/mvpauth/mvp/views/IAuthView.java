package ru.flashrainbow.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import ru.flashrainbow.mvpauth.mvp.presenters.IAuthPresenter;
import ru.flashrainbow.mvpauth.ui.custom_views.AuthPanel;

public interface IAuthView {

    void showMessage(String message);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();

    IAuthPresenter getPresenter();

    void showLoginBtn();

    void hideLoginBtn();

    void showErrorEmailEt();

    void hideErrorEmailEt();

    void showErrorPasswordEt();

    void hideErrorPasswordEt();

    @Nullable
    AuthPanel getAuthPanel();
}
